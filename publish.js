var async = require('async');
var fs = require('fs');
var extend = require('util')._extend;

module.exports = function(options, cb)
{
	options.log.info('Publishing Dnx Console App To: %s', options.s3Bucket);

	async.waterfall([
		async.apply(options.dnxConsoleFn.gitVersionDirectory, options),
		packgeIfNotExisting,
		uploadToS3
	], cb);

	options.dnxConsoleFn.packageFilepath(options);
}


function packgeIfNotExisting(options,cb)
{
	if(!fs.existsSync(options.dnxConsoleFn.packageFilepath(options))) {
		return require('./package')(options,cb);
	}

	cb(null, options);
}


function uploadToS3(options,cb)
{

	var file = options.dnxConsoleFn.packageFilepath(options);
	var env  = options.fn.envHelper.read(	options.dnxConsoleFn.packageEnvFilepath(options)  );

	return options.fn.s3.upload({
		keyBase: 'apps',
		file: file,
		bucket: options.s3Bucket,
		name: options.project,
		version: options.version,
		metadata: env
	}, cb);
}
