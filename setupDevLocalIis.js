var fs = require('fs');
var installer = require('./install.js');
var async = require('async');

var baseDir = process.argv[2];

var log = require('winston');

log.level = 'debug';

if(!baseDir){
	console.error('Please set the base directory');
	return;
}

if (!fs.existsSync(baseDir)) {
    console.error('Directory not found');
    return;
}

var webDir = `${baseDir}/web`;

var setupPublicSite = function(cb){

	var publicSiteOptions = {
		environment: 'dev',
		project: 'web',
		host: 'public.localhost.com',
		dir: `${webDir}/src/PublicSite`,
		enable32Bit: true,
		log: log
	}

	installer(publicSiteOptions, cb);
}

var setupAdminSite = function(_, cb) {
	
	var adminSiteOptions = {
		environment: 'dev',
		project: 'adminweb',
		host:'adminweb.localhost.com',
		dir: `${webDir}/src/AdminWeb`,
		enable32Bit: true,
		log: log
	}
	installer(adminSiteOptions, cb);
}

var setupPublicOrganiserDir = function(_, cb) {

	var options = {
		environment: 'dev',
		project: 'web',
		host: 'public.localhost.com',
		dir: `${webDir}/src/Web`,
		vdir: '/organiser',
		log: log
	};

	installer(options, cb);
}



async.waterfall([
	setupPublicSite,
	setupPublicOrganiserDir,
	setupAdminSite
]);
